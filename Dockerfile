FROM debian:11.2

RUN apt-get update && apt-get install -y --no-install-recommends \
	curl=7.74.0-1.3+deb11u3 \
	nginx=1.18.0-6.1+deb11u2 \
	git=1:2.30.2-1 \
    sudo=1.9.5p2-3 \
    # Fixed packet version for trivy
    dpkg=1.20.10 \
    libc-bin=2.31-13+deb11u4\
    libc6=2.31-13+deb11u4\
    libpcre2-8-0=10.36-2+deb11u1\
    libssl1.1=1.1.1n-0+deb11u3\
    zlib1g=1:1.2.11.dfsg-2+deb11u2\
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& addgroup user \
	&& useradd -rm -d /home/user -s /bin/bash -g user -u 1001 user \
    && echo "user ALL=(ALL) NOPASSWD: /usr/sbin/service nginx start" >> /etc/sudoers.d/user

USER user

CMD ["sleep", "infinity"]